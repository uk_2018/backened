/**
 * 
 */
package com.personal.poc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Umesh Yadav
 *
 */
@Entity
@Table(name="EMPLOYEE")
@Setter
@Getter
@NoArgsConstructor
@ToString
public class Employee implements Serializable {

	private static final long serialVersionUID = 2619390673899957642L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;

	@Column(name="NAME",nullable=false)
	private String name;

	@Column(name="SALARY")
	private Double salary;

	@Column(name="DOJ",nullable=false)
	private Date doj;
}
