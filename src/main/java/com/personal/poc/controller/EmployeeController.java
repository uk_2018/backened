/**
 * 
 */
package com.personal.poc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personal.poc.entity.Employee;
import com.personal.poc.service.EmployeeService;

/**
 * @author Umesh Yadav
 *
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/list")
	public ResponseEntity<?> list() {
		return new ResponseEntity<>(this.employeeService.findAll(), HttpStatus.OK);
	}
	
	@PostMapping("/register")
	public ResponseEntity<?> createNewEmployee(@RequestBody Employee employee) {
		return new ResponseEntity<>(this.employeeService.save(employee), HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id")Long id){
		return new ResponseEntity<>(this.employeeService.findById(id),HttpStatus.OK);
	}
}
