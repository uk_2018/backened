/**
 * 
 */
package com.personal.poc.service;

import java.util.List;

import com.personal.poc.entity.Employee;

/**
 * @author Umesh Yadav
 *
 */
public interface EmployeeService {

	/**
	 * @return
	 */
	List<Employee> findAll();

	/**
	 * @param emp
	 * @return
	 */
	Employee save(Employee emp);

	/**
	 * @param empId
	 * @return
	 */
	Employee findById(Long empId);
}
