/**
 * 
 */
package com.personal.poc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personal.poc.entity.Employee;
import com.personal.poc.repository.EmployeeRepository;

/**
 * @author Umesh Yadav
 *
 */
@Service
@Transactional(readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.personal.poc.service.EmployeeService#findAll()
	 */
	@Override
	public List<Employee> findAll() {
		return this.employeeRepository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.personal.poc.service.EmployeeService#save(com.personal.poc.entity.
	 * Employee)
	 */
	@Override
	@Transactional
	public Employee save(Employee emp) {
		return this.employeeRepository.save(emp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.personal.poc.service.EmployeeService#findById(java.lang.Long)
	 */
	@Override
	public Employee findById(Long empId) {
		Optional<Employee> emp = this.employeeRepository.findById(empId);
		return emp.orElse(new Employee());
	}

}
