/**
 * 
 */
package com.personal.poc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.personal.poc.entity.Employee;

/**
 * @author Umesh Yadav
 *
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
